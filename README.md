# WunderTest
---

## Set-Up

1. Clone the project to your local server.
2. Create DB `wunder_db` and user `wunder` that is granted full privileges to the `wunder_db`.
 - If step 2 is not possible, please change config variables in `.env` file accordingly.
3. Rename `.env.example` to `.env`.  
4. `composer install`.
5. Open __2nd console__ in the root folder of the project and run `php artisan migrate:fresh`. This will create necessary tables in the __DB__.
6. Open __console__ in the root folder of the project and run `php artisan serve`. 
7. Congratulations, the set-up is complete! Now you can navigate to `localhost:8000` in the browser of your choice.

---

## Possible performance optimizations

1. __Caching__ config, routes, and queries.
2. __Middleware__ should handle all redirects.
3. __Backend optimization__. Can't do this without having access to the server.

---

## __TODO__: Improvements

1. Clean up the code and better project structure.
2. __PHPunit__ tests.
3. Conversion to full __MVP__ pattern.
4. Instead of using one table `user_data`, there should be `user_data` and `user_payment`. One for registration data, and one for all types of payments by the user. This way there is no more limit of one payment per person due to the user id being unique in the current implementation of single-table `user_data`. 
5. Better `Readme.md`.

---

## Pattern Used

As one can probably see from the project structure it is a mix of a little bit of __MVC__ and __MVP__. Laravel doesn't come with __MVP__ support. However, I wanted to experiment and see if I can introduce the __MVP__ pattern to the framework. I think it was more of a success, and with more tinkering, one could fully convert it to __MVP__ pattern. 





