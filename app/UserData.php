<?php namespace App;

use Traits\PresentableTrait;

use Illuminate\Database\Eloquent\Model;

class UserData extends Model {

    protected $table = 'user_data';

    protected $primaryKey = 'id';

    public $timestamps = false;

    use PresentableTrait;
  
    protected $presenter = 'Presenters\UserDataPresenter';

    protected $fillable = [
        'first_name',
        'last_name',
        'phone',
        'address',
        'zip',
        'city',
        'payment_data_id'
    ];

    public function isFillable($key){
        return in_array($key, $this->fillable);
    }

}