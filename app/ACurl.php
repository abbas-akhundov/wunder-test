<?php

namespace App;

class ACurl{

    public static function sendRegistrationData($id, $iban, $owner){
        $endpoint = "https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data";
        $client = new \GuzzleHttp\Client();

        $response = $client->request('POST', $endpoint, ['json' => [
            'customerId' => $id,
            'iban' => $iban,
            'owner' => $owner
        ]]);

        $statusCode = $response->getStatusCode();
        if($statusCode == 200){
            return json_decode($response->getBody(), true)["paymentDataId"];
        }
    }
}
