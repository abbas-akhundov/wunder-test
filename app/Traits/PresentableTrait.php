<?php namespace Traits;


trait PresentableTrait {

    /* This is where we will save the presenterInstance so use later on the same model object */
      protected $presenterInstance;
  
      public function present(){
  
          if( ! $this->presenter or ! class_exists($this->presenter)){

              throw new \Exception('Please set the Presenter path to your Presenter FQN');
          }
  
          // The good old Singleton pattern
          if( ! $this->presenterInstance ){
              $this->presenterInstance = new $this->presenter($this);
          }
  
          return $this->presenterInstance;
      }
  }