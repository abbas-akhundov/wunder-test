<?php namespace Presenters;

use App\Presenter;

class UserDataPresenter extends Presenter {

    public function fullname(){
      return ucfirst($this->first_name). ' '.ucfirst($this->last_name);
      
    }

    public function firstName(){
        return ucfirst($this->firstName());
    }

    public function lastName(){
        return ucfirst($this->lastName());
    }

    public function registerValue($key,  $value){
        $this->{$key} = $value;
        $this->save();
    }

    public function registerValues(array $keyValuePairs){
        foreach($keyValuePairs as $key => $value){
            if($this->isFillable($key)){
                $this->{$key} = $value;
            }
        }

        $this->save();
    }
}