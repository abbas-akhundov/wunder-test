<?php

namespace App;


abstract class Presenter{

    protected $entity; // This is to store the original model instance

    function __construct($entity){
        $this->entity = $entity;
    }

    // Call the function if that exsits, or return the property on the original model
    public function __get($property){
        if (method_exists($this, $property)) {
            return $this->{$property}();
        }

        return $this->entity->{$property};
    }

    public function __set(string $name, string $value){
        $this->entity->{$name} = $value;
    }

    public function save(){
        $this->entity->save();
    }

    public function isFillable(string $key){
        return $this->entity->isFillable($key);
    }
}
