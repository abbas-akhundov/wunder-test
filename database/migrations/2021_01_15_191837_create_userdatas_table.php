<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserdatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_data', function (Blueprint $table) {
            $table->id()->autoIncrement()->unique();
            $table->string("first_name")->default("")->nullable();
            $table->string("last_name")->default("")->nullable();
            $table->string("phone")->default("")->nullable();
            $table->string("address")->default("")->nullable();
            $table->string("zip")->default("")->nullable();
            $table->string("city")->default("")->nullable();
            $table->string("payment_data_id")->default("")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_data');
    }
}
