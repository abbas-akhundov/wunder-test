<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

    <title>Laravel </title>


    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

    <!-- Styles -->
    <style>
        html,
        body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links>a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        .intl-tel-input {
            display: table-cell;
        }

        .intl-tel-input .selected-flag {
            z-index: 4;
        }

        .intl-tel-input .country-list {
            z-index: 5;
        }

        .input-group .intl-tel-input .form-control {
            border-top-left-radius: 4px;
            border-top-right-radius: 0;
            border-bottom-left-radius: 4px;
            border-bottom-right-radius: 0;
        }
    </style>
</head>

<body>
    <div class="flex-center position-ref full-height">
        @if (Route::has('login'))
        <div class="top-right links">
            @auth
            <a href="{{ url('/home') }}">Home</a>
            @else
            <a href="{{ route('login') }}">Login</a>

            @if (Route::has('register'))
            <a href="{{ route('register') }}">Register</a>
            @endif
            @endauth
        </div>
        @endif

        <div class="content">
            <div class="m-b-md">

                <?php

                use App\ACurl;
                use App\UserData;
                //$id = false;
                $id = session('user_id');
                $lastStep = session('last_step', 0);
                if ($id) {
                } else {
                    $id = UserData::insertGetId(
                        []
                    );
                    session(['user_id' => $id]);
                }



                $userData = UserData::find($id);

                $present = $userData->present();

                if ($present->payment_data_id) {
                    header("Location: /final");
                    exit();
                }

                if (isset($request)) {
                    if ($request->isMethod('post')) {
                        $present->registerValues($request->all());
                        if ($request->get("step") <= $lastStep && $lastStep !== false) {
                            $lastStep++;
                            session(['last_step' => $lastStep]);

                            if (
                                $lastStep == 3 && !$present->payment_data_id && $request->get('iban', false)
                                && $request->get('owner', false)
                            ) {
                                $paymentDataId = ACurl::sendRegistrationData(
                                    $present->id,
                                    $request->get('iban'),
                                    $request->get('owner')
                                );
                                $present->registerValue("payment_data_id", $paymentDataId);
                                header("Location: /final");
                                exit();
                            }  
                        }
                    }
                }

                ?>
                @if($lastStep == 0)
                <form method="POST" action="/">
                    @csrf
                    <div class="row">
                        <div class="form-group col-md-6">
                            <input required type="text" class="form-control" name="first_name" value="{{ $userData->present()->first_name}}" placeholder="First Name">
                            <span class="input-group-addon">First Name</span>
                        </div>
                        <div class="form-group col-md-6">
                            <input required type="text" class="form-control" name="last_name" value="{{ $userData->present()->last_name}}" placeholder="Last Name">
                            <span class="input-group-addon">Last Name</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="country">* What country are you in ?</label>
                        <select name="country" class="form-control">
                            <option data-code="+49" value="ger">Gemany</option>
                            <option data-code="+44" value="uk">United Kingdom</option>
                            <option data-code="+30" value="gre">Greece</option>
                            <option data-code="+33" value="fra">France</option>
                            <option data-code="+39" value="ita">Italy</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="phone">* What’s your phone number ?</label>
                        <input required type="tel" name="phone" value="{{ $userData->present()->phone ? $userData->present()->phone : '+49'}}" class="form-control" />
                    </div>
                    <input type="hidden" id="step" name="step" value="0">
                    <div class="form-button pt-4"> <button type="submit" class="btn btn-primary btn-block" value="Register" name="register"><span>NEXT</span></button> </div>
                </form>
                @elseif ($lastStep == 1)

                <form method="POST" action="/">
                    @csrf
                    <div class="form-group">
                        <input required type="text" class="form-control" name="address" value="{{ $userData->present()->address}}" placeholder="Address">
                        <span class="input-group-addon">Address</span>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <input required type="text" class="form-control" name="zip" value="{{ $userData->present()->zip}}" placeholder="ZIP">
                            <span class="input-group-addon">ZIP</span>
                        </div>
                        <div class="form-group col-md-6">
                            <input required type="text" class="form-control" name="city" value="{{ $userData->present()->city}}" placeholder="City">
                            <span class="input-group-addon">City</span>
                        </div>
                    </div>
                    <input type="hidden" id="step" name="step" value="1">
                    <div class="form-button pt-4"> <button type="submit" class="btn btn-primary btn-block" value="Next" name="Next"><span>NEXT</span></button> </div>
                </form>

                @elseif ($lastStep == 2)
                <form method="POST" action="/">
                    @csrf
                    <div class="form-group">
                        <input required type="text" class="form-control" name="owner" value="" placeholder="Account Owner">
                        <span class="input-group-addon">Owner</span>
                    </div>

                    <div class="form-group">
                        <input required type="text" class="form-control" name="iban" value="" placeholder="IBAN">
                        <span class="input-group-addon">IBAN</span>
                    </div>


                    <input type="hidden" id="step" name="step" value="2">
                    <div class="form-button pt-4"> <button type="submit" class="btn btn-primary btn-block" value="Next" name="Next"><span>NEXT</span></button> </div>
                </form>
                @endif
            </div>
        </div>
    </div>
</body>

<script>
    $(document).ready(function() {
        $("[name='country']").on("change", function() {
            $("[name='phone']").val($(this).find("option:selected").data("code"));
        });
    });
</script>

</html>